package com.marcusblackhall.blog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest (classes = BlogApplication.class)
public class BlogApplicationTests {

    @Autowired
    private BlogSingleton blogSingleton;

    @Test
    public void shouldShowThatAutowireInjectsSamePrototypeInSingleton() {

        assertNotNull(blogSingleton);

        BlogPrototype prototype1 = blogSingleton.getPrototype();
        BlogPrototype prototype2 = blogSingleton.getPrototype();

        BlogPrototype prototype3 = blogSingleton.getPrototypeFromObjectFactory();
        BlogPrototype prototype4 = blogSingleton.getPrototypeFromObjectFactory();

        assertEquals(prototype1, prototype2);
        assertNotEquals(prototype3, prototype4);

    }

}
