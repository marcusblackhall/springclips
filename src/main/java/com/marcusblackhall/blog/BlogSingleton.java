package com.marcusblackhall.blog;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BlogSingleton {

    @Autowired
    BlogPrototype blogPrototype;

    @Autowired
    ObjectFactory<BlogPrototype> blogPrototypeObjectFactory;

    public BlogPrototype getPrototype() {
        return blogPrototype;
    }

    public BlogPrototype getPrototypeFromObjectFactory() {

        return blogPrototypeObjectFactory.getObject();
    }

}
