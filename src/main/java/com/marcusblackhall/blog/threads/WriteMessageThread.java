package com.marcusblackhall.blog.threads;

import java.time.LocalDateTime;

public class WriteMessageThread implements Runnable {
    @Override
    public void run() {
        System.out.println("Thread is running at : " +  LocalDateTime.now());
    }
}
