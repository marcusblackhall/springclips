package com.marcusblackhall.blog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(BlogSpringConfig.class)
public class BlogApplication {

    static Logger logger = LoggerFactory.getLogger(BlogApplication.class);
    private static ApplicationContext applicationContext;

	public static void main(String[] args) {



	    SpringApplication.run(BlogApplication.class, args);
	    displayAllBeans(args);
	}

    public static void displayAllBeans(String [] args) {
        applicationContext = SpringApplication.run(BlogApplication.class, args);
        String[] allBeanNames = applicationContext.getBeanDefinitionNames();
        for(String beanName : allBeanNames) {
            logger.info(beanName);
        }
    }
}
