###Purpose
Demonstration for how a prototype bean can be injected into a singleton using Spring

####Problem
When a prototype is injected into a singleton bean then a new prototype is not used each time the singleton is referenced.

####Solution

Autowire the Spring ObjectFactory class into the singleton and then a new instance of the prototype can be retrieve from the factory in the singleton,

```java@Autowired
  ObjectFactory<BlogPrototype> blogPrototypeFactory; 
  
  public BlogPrototype getBlogProtype(){
     return blogProotypeFactory.getObject();
  }

```

Where BlogPrototype is defined as a Spring protype bean and it is being used in a singleton bean with the name BlogSingleton.

Classes are define as below 

```java
@Component
public class BlogSingleton {

    @Autowired
    BlogPrototype blogPrototype;

    @Autowired
    ObjectFactory<BlogPrototype> blogPrototypeObjectFactory;

    public BlogPrototype getPrototype() {
        return blogPrototype;
    }

    public BlogPrototype getPrototypeFromObjectFactory() {

        return blogPrototypeObjectFactory.getObject();
    }

}


@Component
@Scope("prototype")
public class BlogPrototype {

}
```
The following test will then pass and demonstrate the problem and solution ..
```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class BlogApplicationTests {

    @Autowired
    private BlogSingleton blogSingleton;

    @Test
    public void contextLoads() {
    }

    @Test
    public void shouldShowThatAutowireInjectsSamePrototypeInSingleton() {

        assertNotNull(blogSingleton);

        BlogPrototype prototype1 = blogSingleton.getPrototype();
        BlogPrototype prototype2 = blogSingleton.getPrototype();

        BlogPrototype prototype3 = blogSingleton.getPrototypeFromObjectFactory();
        BlogPrototype prototype4 = blogSingleton.getPrototypeFromObjectFactory();

        assertEquals(prototype1, prototype2);
        assertNotEquals(prototype3, prototype4);

    }

}

```


    

